# <Sudoku solver>

Napište sudoku solver. Na vstupu dostanete hrací desku s libovolným počtem předvyplněných buněk. Vaším úkolem je vyplnit Sudoku, pokud řešení existuje.

Minimální požadavky

Vaše aplikace musí umět alespoň:

    Načíst a interně zareprezentovat sudoku.
    Najít alespoň jedno řešení (existuje-li) libovolně zadaného sudoku 9x9.

Extra body:
    Implementováno - Velikost hrací desky není omezená {n^2 X n^2 ∣ n≥1}
## Example usage

See SudokuSolver.rkt and internalSudoku-test.rkt for more sudoku examples

Důležité jsou dvě funkce parseSudoku -> vrací interní reprezentaci sudoku nebo False, pokud je již bez nutnosti hádat statickou analýzou jednoznačně neřešitelné či byl vstup neplatný.
Důležité jsou dvě funkce solveSudoku -> vrací false, pokud řešení neexistuje anebo seznam řádek hodnot, vyřešeného sudoku.

Pro vstup požaduji sudoku jakožto list řádek 
každá řádka je list [celých čísel - tyto hodnoty jsou brány jako přednastavené hodnoty] (takové hodnoty jsou přednastavené)
                    [cokoliv jiného - tuto pozici doplň]

Typické použití bude vypadat asi následovně *Disclaimer: formátování výstupu musí člověk udělat manuálně*:
```
(require "internalSudoku.rkt")

(define exampleD2 '(
    [_ 1  _ 2 ]
    [2 _  _ _ ]
    [3 _  1 4 ]
    [_ _  2 _ ]
))
(solveSudoku (parseSudoku exampleD2))
#|
'((4 1  3 2) 
  (2 3  4 1) 
  (3 2  1 4)
  (1 4  2 3))
|#

(define exampleD4 '( 
    [() 1  () ()  () () () ()  () () () ()  () () 5  ()]
    [() () 2  ()  () () () ()  () () () ()  () () () ()]
    [() () () ()  () () () ()  () () () ()  () () () 6 ]
    [() () () 3   () () () ()  () () () ()  () () () ()]

    [() () () ()  () () () ()  () () 7  ()  () () () ()]
    [() () () ()  () () 16 ()  () () () ()  () () () ()]
    [() () () ()  () () () ()  () () () ()  () () () ()]
    [() () () ()  () () () ()  () () () ()  () () () ()]

    [() () () ()  () () () ()  () 10 () ()  () () () ()]
    [() () () ()  () () () ()  () () () ()  () () () ()]
    [() () () ()  () 11 () 12  () () () ()  () () () ()]
    [() () () ()  () () () ()  () () () ()  () () () ()]
    
    [() () () ()  () () () ()  () () () ()  () () () ()]
    [() () () ()  () () () ()  () () () ()  () () () ()]
    [() () () ()  () () () ()  () () () ()  () () () ()]
    [() () () ()  () () () ()  () () () ()  () () () ()])
)
(solveSudoku (parseSudoku exampleD4))
#|
'((4 1 6 7 10 2 3 8 11 9 12 13 14 15 5 16)
 (5 8 2 9 13 15 12 16 3 4 6 14 1 7 10 11)
 (10 11 12 13 7 9 1 14 8 15 5 16 2 3 4 6)
 (14 15 16 3 6 4 11 5 7 1 2 10 8 9 12 13) 
 (6 9 8 15 11 1 2 3 16 5 7 4 10 12 13 14)
 (3 7 10 1 12 5 16 4 13 6 14 9 11 2 15 8)
 (12 13 4 5 14 6 8 9 10 2 11 15 7 1 16 3) 
 (2 14 11 16 15 7 13 10 1 8 3 12 5 4 6 9)
 (13 6 9 8 1 3 7 2 12 10 15 5 16 14 11 4)
 (11 12 5 14 4 8 9 6 2 16 1 7 13 10 3 15)
 (15 16 7 10 5 11 14 12 4 3 13 6 9 8 1 2) 
 (1 2 3 4 16 10 15 13 9 14 8 11 6 5 7 12)
 (16 5 13 6 2 12 4 1 14 7 9 3 15 11 8 10)
 (8 10 14 11 3 13 5 7 15 12 16 2 4 6 9 1)
 (9 4 15 12 8 14 6 11 5 13 10 1 3 16 2 7)
 (7 3 1 2 9 16 10 15 6 11 4 8 12 13 14 5))
|#

(define invalidD3_1 '( 
    [() () ()  () () ()  () () ()]
    [() () ()  () () 3   () 8  5 ] 
    [ 1 () 1   () 2 ()   () () ()]

    [() () ()  5  () 7   () () ()]
    [() () 4   () () ()  1  () ()]
    [() 9  ()  () () ()  () () ()]

    [5  () ()  () () ()  ()  7  3]
    [() ()  2  ()  1 ()  () () ()]
    [() () ()  ()  4 ()  () ()  9]) 
)
(parseSudoku invalidD3_1) #f
```